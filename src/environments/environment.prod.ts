export const environment = {
  firebase: {
    apiKey: "AIzaSyBqe785kh-CTUqw-B1JgOupQYGJOj4e4ho",
    authDomain: "asemupez-uned.firebaseapp.com",
    databaseURL: "https://asemupez-uned.firebaseio.com",
    projectId: "asemupez-uned",
    storageBucket: "asemupez-uned.appspot.com",
    messagingSenderId: "870135376857"
  },
  api: 'http://localhost:4000/',
  production: true
};
