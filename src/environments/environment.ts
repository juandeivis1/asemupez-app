// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyBqe785kh-CTUqw-B1JgOupQYGJOj4e4ho",
    authDomain: "asemupez-uned.firebaseapp.com",
    databaseURL: "https://asemupez-uned.firebaseio.com",
    projectId: "asemupez-uned",
    storageBucket: "asemupez-uned.appspot.com",
    messagingSenderId: "870135376857"
  },
  api: 'http://localhost:4000/',
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
