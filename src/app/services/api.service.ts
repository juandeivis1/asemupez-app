import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { socioData, getMovimientos, buscarSocio, movimientosCredito, registrados } from './query'
import { DefinirSocio, registrarTramite } from './mutation'
import { AngularFirestore } from '@angular/fire/firestore';
import { catchError } from 'rxjs/operators';
import { of } from 'zen-observable';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private apollo: Apollo,
    private fs: AngularFirestore
  ) { }

  context = (token) => {
    return {
      headers: {
        Authorization: `Bearer ${token}`
      }
    };
  }

  socio = (token) => this.apollo.query<any>({
    query: socioData,
    context: this.context(token)
  })

  getMovimientos = (token, codigo) => this.apollo.query<any>({
    query: getMovimientos,
    context: this.context(token),
    variables: { codigo }
  })

  movimientosCredito = (token, codigo) => this.apollo.query<any>({
    query: movimientosCredito,
    context: this.context(token),
    variables: { codigo }
  })

  buscarSocio = (token, email) => this.apollo.query<any>({
    query: buscarSocio,
    context: this.context(token),
    variables: { email }
  })

  registrados = (token) => this.apollo.query<any>({
    query: registrados,
    context: this.context(token),
  })

  definirSocio = (token, uid, socio) => this.apollo.mutate({
    mutation: DefinirSocio,
    context: this.context(token),
    variables: { uid, socio }
  })

  registrarTramite = (token, id, name) => this.apollo.mutate({
    mutation: registrarTramite,
    context: this.context(token),
    variables: { id, name }
  })

  getAdmin = (uid) => this.fs.collection('admin').doc(uid).valueChanges().pipe(catchError(_ => of(null)))

  addAgreement = (agreement) => this.fs.collection('agreements').doc(agreement.id).set(agreement);

  updateAgreement = (agreement) => this.fs.collection('agreements').doc(agreement.id).update(agreement);

}
