import gql from 'graphql-tag'

const socioData = gql`
    query {
        user {
            id
            name
            identification
            address
            birth
            productos {
                monto
                codigo {
                    id
                    descripcion
                }
                porcentaje
                cuota
                interes
                pagos
                movimiento
            }
            creditos {
                id
                solicitud
                formalizado
                montoAprovado
                cuota
                cuotas
                tasaInteres
                cargos
                pagos
                saldo
                ultimo
                desembolsado
                tipo {
                    codigo
                    detalle
                }
            }
        }
    }`

const getMovimientos = gql`
    query getMovimientos($codigo: Int){
        movimientos: getMovimientos(codigo: $codigo) {
            id
            fechaMovimiento
            numPago
            montoMovimiento
            montoPrincipal
        }
    }`;


const buscarSocio = gql`
    query buscarSocio($email: String){
        socio: buscarSocio(email: $email) {
            uid
            email
            displayName
            photoURL
            socio {
                id
                name
                identification
                address
                birth
            }
        }
    }`;

const movimientosCredito = gql`
    query movimientosCredito($codigo: String){
        movimientos: movimientosCredito(codigo: $codigo) {
            id
            montoMovimiento
            fechaMovimiento
            numPago
            montoPrincipal
        }
    }`;

const registrados = gql`
    query registrados{
        registrados: registrados {
            uid
            email
            displayName
            photoURL
            socio
        }
    }`;

export {
    socioData,
    getMovimientos,
    buscarSocio,
    movimientosCredito,
    registrados
}