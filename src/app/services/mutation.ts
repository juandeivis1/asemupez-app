import gql from 'graphql-tag'

const DefinirSocio = gql`
    mutation definirSocio($uid: String, $socio: Int ){
        id: definirSocio(uid: $uid, socio: $socio) {
            uid
            email
            displayName
            photoURL
            socio {
            id
            name
            identification
            address
            birth
            }
        }
    }`;

const registrarTramite = gql`
    mutation registrarTramite ($id: Int, $name: String){
    registrarTramite(id: $id, name: $name)
    }`;

export { DefinirSocio, registrarTramite }