import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { Store, Select, Actions, ofActionSuccessful } from '@ngxs/store';
import { BuscarSocio, AdminState, DefinirSocio, Registrados } from '../../states/admin.state';
import { tap } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { LoadingComponent } from 'src/app/core/loading/loading.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  displayedColumns: string[] = ['displayName', 'photoURL', 'socio', 'email',];
  dataSource: MatTableDataSource<any>;
  @Select(AdminState.registrados) registrados: Observable<any[]>
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  email: FormControl;
  socioForm: FormGroup;
  socio;
  loading: MatDialogRef<any>;
  constructor(
    private fb: FormBuilder,
    private store: Store,
    private dialog: MatDialog,
    private actions: Actions
  ) {
    this.actions.pipe(ofActionSuccessful(BuscarSocio, DefinirSocio)).subscribe(() => {
      this.dialog ? this.loading.close() : null;
    })

  }

  ngOnInit() {
    this.registrados.pipe(
      tap(
        registrados => {
          this.dataSource = new MatTableDataSource(registrados);
        }
      )
    ).subscribe();
    this.email = this.fb.control(null, [Validators.required, Validators.email])
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.store.dispatch(new Registrados())
  }

  buscar = (email?) => {
    this.loading = this.dialog.open(LoadingComponent, { data: 'Buscando usuario' });
    this.store.dispatch(new BuscarSocio(email)) 
    this.socio = this.store.select(AdminState.socio).pipe(tap(user => {
      if (user) {
        this.socioForm = this.fb.group({
          uid: [user.uid, [Validators.required]],
          socio: [ null, [Validators.required]],
        })
        if (user.socio && user.socio.id ) {
          this.socioForm.patchValue({ socio: user.socio.id })
        }
      }
    }))
  }

  definir = () => {
    this.loading = this.dialog.open(LoadingComponent, { data: 'Ligando socio a cuenta' });
    this.store.dispatch(new DefinirSocio(this.socioForm.value));
  }

}
