import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormalityCardComponent } from './formality-card.component';

describe('FormalityCardComponent', () => {
  let component: FormalityCardComponent;
  let fixture: ComponentFixture<FormalityCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormalityCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormalityCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
