import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-formality-card',
  templateUrl: './formality-card.component.html',
  styleUrls: ['./formality-card.component.css']
})
export class FormalityCardComponent implements OnInit {
  @Input() formality: any;
  amount: FormControl


  types = [
    { id: 1, type: 'Nuevo monto ahorro navideño' },
    { id: 2, type: 'Vale de dinero' },
    { id: 3, type: 'Nuevo monto ahorro escolar' },
  ];

  states = [
    { id: 1, state: 'En espera' },
    { id: 2, state: 'Cancelado' },
    { id: 3, state: 'Aprobado' },
    { id: 4, state: 'Rechazado' },
    { id: 5, state: 'Contraofertado' },
    { id: 6, state: 'Aceptado por socio' },
    { id: 7, state: 'Rechazado por socio' },
  ];s

  constructor(
    private fb: FormBuilder,
    private fs: AngularFirestore
  ) { }

  ngOnInit() {
    this.amount = this.fb.control(null, [Validators.required])
  }

  getType = (id) =>
    this.types.find(state => state.id === id).type

  getState = (id) =>
    this.states.find(state => state.id === id).state

  from = (date) => moment(date).fromNow()

  changeState = (formality, state) => {
    this.fs.collection('formalities').doc(formality.id).update({ ts: Date.now(), state })
  }

  setAmount = () => {
    this.fs.collection('formalities').doc(this.formality.id).update({ ts: Date.now(), state: 5, offer: this.amount.value })
  }

}
