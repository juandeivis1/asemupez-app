import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { AgreementsComponent } from './agreements/agreements.component';
// import { ProductsComponent } from './products/products.component';
// import { ServicesComponent } from './services/services.component';
// import { AgreementsComponent } from './agreements/agreements.component';
// import { FormalitiesComponent } from './formalities/formalities.component';

const routes: Routes = [
  { path: 'user', component: UserComponent },
  { path: 'agreements', component: AgreementsComponent },
  { path: '', component: DashboardComponent},
  { path: '**', redirectTo: 'services' }

  // { path: 'register', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
