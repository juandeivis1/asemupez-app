import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, combineLatest, BehaviorSubject, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import * as moment from 'moment';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-agreements',
  templateUrl: './agreements.component.html',
  styleUrls: ['./agreements.component.css']
})
export class AgreementsComponent implements OnInit {
  stateSelected= new BehaviorSubject(null);
  typeSelected = new BehaviorSubject(null);
  states = [
    { id: 1, state: 'En espera' },
    { id: 2, state: 'Cancelado' },
    { id: 3, state: 'Aprobado' },
    { id: 4, state: 'Rechazado' },
  ];

  types = [
    { id: 1, type: 'Nuevo monto ahorro navideño' },
    { id: 2, type: 'Vale de dinero' },
    { id: 3, type: 'Nuevo monto ahorro escolar' },
  ];
  formalities: Observable<any>;
  amount: FormControl
  constructor(
    private fs: AngularFirestore,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  moment.locale('es')
  this.formalities = combineLatest([this.typeSelected, this.stateSelected])
    .pipe(
      switchMap(([type, state]) => {
        if (type && state) {
          return this.fs
            .collection('formalities',
              _formalities => _formalities.where('type', '==', type.id).where('state', '==', state.id).orderBy( state == 1 ? 'ts' : 'created', state == 1 ? 'asc' : 'desc').limit(5)
            ).valueChanges()
        } else {
          return of(null);
        }
      })
    )
    this.amount = this.fb.control(null, [Validators.required])
  }

  selectState = (state) => {
    this.stateSelected .next(state);
  }

  selectType = (type) => {
    if (type && type.id == 2) {
      this.states = [
        { id: 1, state: 'En espera' },
        { id: 2, state: 'Cancelado' },
        { id: 3, state: 'Aprobado' },
        { id: 4, state: 'Rechazado' },
        { id: 5, state: 'Contraofertado' },
        { id: 6, state: 'Oferta aceptado' },
        { id: 7, state: 'Oferta rechazada' },
      ]
    } else {
      this.states = [
        { id: 1, state: 'En espera' },
        { id: 2, state: 'Cancelado' },
        { id: 3, state: 'Aprobado' },
        { id: 4, state: 'Rechazado' },
      ]
    }
    this.typeSelected.next(type);
  }

  from = (date) => moment(date).fromNow()

  get Type () {
    return this.typeSelected.value;
  }

  get State () {
    return this.stateSelected.value;
  }

  getState = (id) =>
    this.states.find(state => state.id === id).state

  getType = (id) =>
    this.types.find(state => state.id === id).type

  changeState = (formality, state) => {
    this.fs.collection('formalities').doc(formality.id).update({ ts: Date.now(), state })
  }

}
