import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';

import { Store } from '@ngxs/store';
import { UserState } from '../states/user.state';
import { switchMap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store,
    private router: Router,
    private afAuth: AngularFireAuth
  ) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.afAuth.authState.pipe(
      switchMap((userState) => {
        if (userState) {
          return of(true);
        } else {
          this.router.navigate(['/login']);
          return of(false);
        }
      })
    )

  }
}
