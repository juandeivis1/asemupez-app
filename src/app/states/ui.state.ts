import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Platform } from '@angular/cdk/platform';


export interface UI {
    cellar: View
    inventory: View
    product?: View
    device?: Platform
    selectedColor?: string
    install?: any
    share?: boolean
    sidenav?: any
}

interface View {
    list: boolean
}

export class ToggleCellarView {
    static readonly type = '[UI] ToggleCellarView]';
}

export class ToggleInventoryView {
    static readonly type = '[UI] ToggleInventoryView]';
}

export class ToggleProductView {
    static readonly type = '[UI] ToggleProductView]';
}

export class SelectedColor {
    static readonly type = '[UI] SelectedColor]';
    constructor(public readonly color: string) { }
}

export class SetDrawer {
    static readonly type = '[UI] SetDrawer]';
    constructor(public readonly drawer: any) { }
}

export class InstallApp {
    static readonly type = '[UI] InstallApp]';
}

@State<UI>({
    name: 'ui',
    defaults: {
        cellar: {
            list: true
        },
        inventory: {
            list: false
        },
        product: {
            list: false
        }
    }
})
export class UIState {

    @Selector()
    static device(state: UI) {
        return state.device;
    }

    @Selector()
    static selectedColor(state: UI) {
        return state.selectedColor;
    }

    @Selector()
    static product(state: UI) {
        return state.product;
    }

    @Selector()
    static install(state: UI) {
        return state.install;
    }

    @Selector()
    static share(state: UI) {
        return state.share;
    }


    constructor(public platform: Platform) {

    }

    async ngxsOnInit({ patchState }: StateContext<UI>) {
        const newVariable: any = window.navigator;
        let share = false;
        if (newVariable.share) {
            share = true;
        }
        window.addEventListener('beforeinstallprompt', (install: any) => {
            console.log('beforeinstallprompt Event fired');
            install.preventDefault();
            patchState({ install });
        });
        patchState({ device: { ...this.platform }, share });
    }

    @Action(ToggleCellarView)
    toggleCellarView({ patchState, getState }: StateContext<UI>) {
        const { cellar: { list } } = getState();
        patchState({ cellar: { list: !list } });
    }

    @Action(ToggleInventoryView)
    toggleInventoryView({ patchState, getState }: StateContext<UI>) {
        const { inventory: { list } } = getState();
        patchState({ inventory: { list: !list } });
    }

    @Action(ToggleProductView)
    ToggleProductView({ patchState, getState }: StateContext<UI>) {
        const { product: { list } } = getState();
        patchState({ product: { list: !list } });
    }

    @Action(SelectedColor)
    SelectedColor({ patchState }: StateContext<UI>, { color }: SelectedColor) {
        patchState({ selectedColor: color });
    }

    @Action(SetDrawer)
    SetDrawer({ patchState }: StateContext<UI>, { drawer }: SetDrawer) {

        patchState({ sidenav: drawer });
    }

    @Action(InstallApp)
    async installApp({ getState, patchState }: StateContext<UI>) {
        const { install } = getState();
        if (install !== undefined) {
            install.prompt();

            const choiceResult = await install.userChoice;

            console.log(choiceResult.outcome);

            if (choiceResult.outcome == 'dismissed') {
                console.log('User cancelled home screen install');
            }
            else {
                console.log('User added to home screen');
                patchState({ install: null })
            }
        }
    }
}
