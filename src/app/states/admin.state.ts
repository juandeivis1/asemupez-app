import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { ApiService } from '../services/api.service';
import { tap } from 'rxjs/operators';
import { UserState } from './user.state';

export class AdminStateModel {
  socio: any;
  registrados: any[];
}

export class BuscarSocio {
  static readonly type = '[Admin] BuscarSocio]';
  constructor(public readonly email: any) {}
}


export class Registrados {
  static readonly type = '[Admin] Registrados]';
}

export class DefinirSocio {
  static readonly type = '[Admin] DefinirSocio]';
  constructor(public readonly socioData: any) {}
}


@State<AdminStateModel>({
  name: 'admin',
  defaults: null
})

export class AdminState {
  constructor(
    private api: ApiService,
    private store: Store
  ) {
  }

  ngxsOnInit({ setState }: StateContext<AdminStateModel>) {
    return setState(null)
  }

  @Selector()
  static socio(state: AdminStateModel) {
    return state.socio;
  }

  @Selector()
  static registrados({registrados}: AdminStateModel) {
    return registrados;
  }

  @Action(Registrados)
  registrados({  patchState }: StateContext<AdminStateModel>) {
    const token = this.store.selectSnapshot(UserState.token);
    return this.api.registrados(token).pipe(tap(({ data: { registrados } }) => {  
      patchState({ registrados });
    }))
  }

  @Action(BuscarSocio)
  buscarSocio({  patchState }: StateContext<AdminStateModel>, { email }: BuscarSocio) {
    const token = this.store.selectSnapshot(UserState.token);
    return this.api.buscarSocio(token, email).pipe(tap(({ data: { socio } }) => {  
      patchState({ socio });
    }))
  }

  @Action(DefinirSocio)
  definirSocio({ patchState, getState }: StateContext<AdminStateModel>, { socioData }: DefinirSocio) {
    const token = this.store.selectSnapshot(UserState.token);
    const { uid, socio } = socioData;
    return this.api.definirSocio(token, uid, socio).pipe(tap(({ data: { id } }) => {
      const { socio } = getState();
      patchState({ socio: id });
    }))
  }

}
