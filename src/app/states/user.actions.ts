
class Login {
  static readonly type = '[User] Login';
  constructor(public readonly credentials: { email: string, password: string }) { }
}

class LoginGoogle {
  static readonly type = '[User] LoginGoogle';
}

class LoginFacebook {
  static readonly type = '[User] LoginFacebook';
}

class Logout {
  static readonly type = '[User] Logout';
}

class SocioQuery {
  static readonly type = '[User] Socio Query';
  constructor(public readonly token: string) { }
}

class MovimientosQuery {
  static readonly type = '[User] Movimientos Query';
  constructor(public readonly codigo: number) { }
}

class MovimientosCreditoQuery {
  static readonly type = '[User] MovimientosCredito Query';
  constructor(public readonly codigo: string) { }
}

class UserInfo {
  static readonly type = '[User] UserInfo';
  constructor(public readonly userInfo: any) { }
}

class AddAgreement {
  static readonly type = '[Admin] AddAgreement';
  constructor(public readonly agreement: any) { }
}

class UpdateAgreement {
  static readonly type = '[Admin] UpdateAgreement';
  constructor(public readonly agreement: any) { }
}

class RegistrarTramite {
  static readonly type = '[Admin] registrarTramite';
}

export {
  SocioQuery,
  Login,
  Logout,
  LoginGoogle,
  LoginFacebook,
  MovimientosQuery,
  UserInfo,
  MovimientosCreditoQuery,
  UpdateAgreement,
  AddAgreement,
  RegistrarTramite
}
