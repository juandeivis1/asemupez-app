import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  SocioQuery,
  Login,
  Logout,
  LoginGoogle,
  LoginFacebook,
  MovimientosQuery,
  UserInfo,
  MovimientosCreditoQuery,
  UpdateAgreement,
  AddAgreement,
  RegistrarTramite
} from './user.actions';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { ApiService } from '../services/api.service';
import { tap, finalize, take } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';


export class UploadImage {
  static readonly type = '[Admin] UploadImage]';
  constructor(
    public readonly file: any,
    public readonly id: string,
    public readonly img?: any,
  ) { }
}

export class SaveImageRoute {
  static readonly type = '[Admin] SaveImageRoute]';
  constructor(
    public readonly fileRef: Observable<any>,
    public readonly id: string,
    public readonly FirebaseId: String
  ) { }
}


export class UserStateModel {
  id: number;
  uid: string;
  email: string;
  identification: string;
  address: string;
  birth: string;
  name: string
  photoURL: string;
  admin: boolean;
  token: string;
  movements: Movement[];
  productos: Producto[];
  creditos: Credito[]
}

export interface Movement {
  id: number
  fechaMovimiento: Date
  montoPrincipal: number
  montoMovimiento: number
  numPago: number
}


export interface Producto {
  monto: number
  codigo: Codigo
  porcentaje: number
  cuota: number
  interes: number
  pagos: number
  movimiento: Date
}

export interface Credito {
  id: string
  solicitud: string
  formalizado: Date
  montoAprovado: number
  cuota: number
  cuotas: number
  cargos: number
  saldo: number
  pagos: number
  ultimo: Date
  desembolsado: number
  tasaInteres: number
  tipo: Linea
}

export interface Linea {
  codigo: number
  detalle: string
}

export interface Codigo {
  id: number
  descripcion: string
}

@State<UserStateModel>({
  name: 'user',
  defaults: null
})

export class UserState {
  constructor(
    private afAuth: AngularFireAuth,
    private api: ApiService,
    private storage: AngularFireStorage,
    private snack: MatSnackBar

  ) {
  }

  ngxsOnInit({ dispatch }: StateContext<UserStateModel>) {
    return this.afAuth.authState.pipe(tap((user) => {
      if (user) {
        dispatch(new UserInfo(user))
        user.getIdToken(true).then(token => {
          dispatch(new SocioQuery(token))
        })
      }
    })).subscribe()
  }

  @Selector()
  static user(state: UserStateModel) {
    return state;
  }

  @Selector()
  static socio(state: UserStateModel) {
    return state.id;
  }

  @Selector()
  static admin(state: UserStateModel) {
    return state.admin;
  }

  @Selector()
  static token(state: UserStateModel) {
    return state.token;
  }

  @Selector()
  static movements(state: UserStateModel) {
    return state.movements;
  }

  @Action(Login)
  login({ dispatch }: StateContext<UserStateModel>, { credentials: { email, password } }: Login) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(() => {
      dispatch(new Navigate(['/dashboard/services']))
    }).catch(() =>{
      this.snack.open('Datos incorrectos', 'Ok', { duration: 3000})
    });
  }

  @Action(LoginGoogle)
  loginGoogle({ dispatch }: StateContext<UserStateModel>) {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
      .then(() => {
        dispatch(new Navigate(['/dashboard/services']))
      });
  }

  @Action(LoginFacebook)
  async loginFacebook({ }: StateContext<UserStateModel>) {
    await this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
  }

  @Action(Logout)
  async logout({ setState, dispatch }: StateContext<UserStateModel>) {
    await this.afAuth.auth.signOut();
    dispatch(new Navigate(['login']))
    setState(null);
  }

  @Action(SocioQuery)
  socioQuery({ patchState }: StateContext<UserStateModel>, { token }: SocioQuery) {
    return this.api.socio(token).pipe(tap(({ data }) => {
      if (data) {
        const { user } = data;
        patchState({ ...user, token });
      }
    }))
  }

  @Action(MovimientosQuery)
  movimientosQuery({ getState, patchState }: StateContext<UserStateModel>, { codigo }: MovimientosQuery) {
    const { token } = getState();
    return this.api.getMovimientos(token, codigo).pipe(
      tap(({ data: { movimientos } }) => {
        patchState({ movements: movimientos })
      })
    )
  }

  @Action(MovimientosCreditoQuery)
  movimientosCreditoQuery({ getState, patchState }: StateContext<UserStateModel>, { codigo }: MovimientosCreditoQuery) {
    const { token } = getState();
    return this.api.movimientosCredito(token, codigo).pipe(
      tap(({ data: { movimientos } }) => {
        patchState({ movements: movimientos })
      })
    )
  }

  @Action(UserInfo)
  userInfo({ dispatch, patchState }: StateContext<UserStateModel>, { userInfo }: UserInfo) {
    const { uid, email, photoURL } = userInfo;
    patchState({ uid, email, photoURL })
    return this.api.getAdmin(uid).pipe(
      tap((userAdmin: any) => {
        if (userAdmin) {
          patchState({ admin: userAdmin.admin });
        }
      })
    )
  }

  @Action(RegistrarTramite)
  registrarTramite({ dispatch, patchState, getState }: StateContext<UserStateModel>) {
    const { id, name, token } = getState();
    return this.api.registrarTramite(token, id, name)
  }

  @Action(AddAgreement)
  async addAgreement({ getState, patchState }: StateContext<UserStateModel>, { agreement }: AddAgreement) {
    const newAgreement = await this.api.addAgreement(agreement);
  }

  @Action(UpdateAgreement)
  async updateAgreement({ getState, patchState }: StateContext<UserStateModel>, { agreement }: UpdateAgreement) {
    return await this.api.updateAgreement(agreement).then(() => {
      console.log('success')
    });
  }

  @Action(UploadImage)
  uploadImage(
    { dispatch }: StateContext<UserStateModel>,
    { file, id, img }: UploadImage
  ) {
    const FirebaseId = `asemupez/agreement-${id}`;
    const fileRef = this.storage.ref(FirebaseId);
    const uploadTask = this.storage.upload(
      `asemupez/agreement-${id}`,
      file,
      { customMetadata: { id } }
    );
    return uploadTask
      .snapshotChanges()
      .pipe(
        finalize(() => {
          dispatch(
            new SaveImageRoute(
              fileRef.getDownloadURL(),
              id,
              FirebaseId
            )
          )
          if (img) {
            this.snack.open('Imagen reemplazada :)', 'Ok', { duration: 3000 })
          } else {
            this.snack.open('Imagen agregada', 'Ok', { duration: 3000 })
          }
        }
        )
      );
  }

  @Action(SaveImageRoute)
  saveImageRoute(
    { dispatch }: StateContext<UserStateModel>,
    {
      fileRef,
      id,
      FirebaseId
    }: SaveImageRoute
  ) {
    return fileRef.pipe(
      take(1),
      tap(url => {
        this.api.updateAgreement({ id, img: { url, FirebaseId, valid: true } })
      })
    );
  }

}
