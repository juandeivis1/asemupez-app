import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Login, Logout, LoginFacebook, LoginGoogle } from 'src/app/states/user.actions';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {MatDialog } from '@angular/material';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8)]]
    })
  }

  loginGoogle = ( ) => this.store.dispatch( new LoginGoogle());

  loginFacebook = ( ) => this.store.dispatch( new LoginFacebook());

  register = ( ) => this.dialog.open(RegisterComponent, { disableClose: true, })

  login = ( ) => this.store.dispatch( new Login(this.loginForm.value));

  logout = () => this.store.dispatch( new Logout())

}
