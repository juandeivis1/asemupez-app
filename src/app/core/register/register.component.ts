import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  nuevoUsuario: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AngularFireAuth,
    public dialogRef: MatDialogRef<RegisterComponent>,
    private snack: MatSnackBar
  ) { }

  ngOnInit() {
    this.nuevoUsuario = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.minLength(8), Validators.required]],
    })
  }

  register = () => {
    const newUser = this.nuevoUsuario.value;
    this.auth.auth.createUserWithEmailAndPassword(newUser.email, newUser.password).then(() => {
      this.dialogRef.close()
      this.snack.open('Usurio registrado con exito', 'Ok', { duration: 3000})
    }).catch(() => {
      this.snack.open('Error el crear el usuario', 'Ok', { duration: 3000})
    })
  }

}
