import { Component } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Login, Logout } from './states/user.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private store: Store
    ) {
  }

  logout = () => this.store.dispatch( new Logout())

}
