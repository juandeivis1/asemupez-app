import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { LoggedGuard } from './guards/logged.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [ LoggedGuard ]},
  { path: 'dashboard', loadChildren: './user/user.module#UserModule', canActivate: [AuthGuard]  },
  // { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [AdminGuard]  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

