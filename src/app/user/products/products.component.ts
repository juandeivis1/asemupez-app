import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState, UserStateModel } from 'src/app/states/user.state';
import { MovimientosQuery, MovimientosCreditoQuery } from 'src/app/states/user.actions';
import { MatBottomSheet } from '@angular/material';
import { MovementsComponent } from '../movements/movements.component';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  user: Observable<UserStateModel>;

  constructor(
    private store: Store,
    private matBottomSheet: MatBottomSheet
  ) { }

  ngOnInit() {
    this.user = this.store.select(UserState.user).pipe(tap(({productos, creditos}) => {

      const productsIds = [2,8];
      const bases = productos.filter(_producto => productsIds.includes(_producto.codigo.id))
      const total = bases.reduce((total, actual) => {
        return total + actual.monto;
      },0);
      const deudas = creditos.reduce((total, actual) => {
        return total + actual.saldo;
      },0);
      const patronal = productos.find(_producto => _producto.codigo.id == 1)
      // console.log((total * 0.9) -deudas);
      // console.log( total * 0.4+patronal.monto * 0.5);
    }) )
  }

  getMovimientos = ({ codigo: { id }}) => {    
    this.store.dispatch(new MovimientosQuery(id));
    this.matBottomSheet.open(MovementsComponent);
  }


  getMovimientosCredito = ({ id }) => {    
    this.store.dispatch(new MovimientosCreditoQuery(id));
    this.matBottomSheet.open(MovementsComponent);
  }


}
