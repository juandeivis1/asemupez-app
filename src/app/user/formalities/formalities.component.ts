import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store, Select } from '@ngxs/store';
import { UserState } from 'src/app/states/user.state';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { of } from 'zen-observable';
import { MatDialog } from '@angular/material';
import { NewFormalityComponent } from '../new-formality/new-formality.component';
import * as moment from 'moment';

@Component({
  selector: 'app-formalities',
  templateUrl: './formalities.component.html',
  styleUrls: ['./formalities.component.css']
})
export class FormalitiesComponent implements OnInit {
  formalities: any;
  @Select(UserState.admin) admin: Observable<boolean>
  userCapacity: Observable<{ savings: number; personal: number; mortgage: number }>;
  capacity = 0
  states = [
    { id: 1, state: 'En espera' },
    { id: 2, state: 'Cancelado' },
    { id: 3, state: 'Aprobado' },
    { id: 4, state: 'Rechazado' },
  ]

  types = [
    { id: 1, type: 'Nuevo monto ahorro navideño' },
    { id: 2, type: 'Vale de dinero' },
    { id: 3, type: 'Nuevo monto ahorro escolar' },
  ]
  stateSelected= new BehaviorSubject(null);
  typeSelected = new BehaviorSubject(null);
  constructor(
    private fs: AngularFirestore,
    private store: Store,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    moment.locale('es')

    this.formalities = combineLatest([this.typeSelected, this.stateSelected])
    .pipe(
      switchMap(([type, state]) => {
        if (type && state) {
          return this.fs
            .collection('formalities',
              _formalities => _formalities.where('type', '==', type.id).where('state', '==', state.id).orderBy( state == 1 ? 'ts' : 'created', state == 1 ? 'asc' : 'desc').limit(5)
            ).valueChanges()
        } else {
          return of(null);
        }
      }),
      tap((formalities: any) => {
        if (formalities) {
          const x = formalities.reduce((total, current) => {
            if ( current.state == 1) {
              total += 1;
            } 
            return total;
          }, 0)
          this.capacity = x;
        }
      })
    )
 
    
    this.userCapacity = this.store.select(UserState.user).pipe(switchMap(({ productos, creditos }) => {
      const productsIds = [2, 8];
      const bases = productos.filter(_producto => productsIds.includes(_producto.codigo.id))
      const total = bases.reduce((total, actual) => {
        return total + actual.monto;
      }, 0);

      const deudasIds = [1, 4, 5, 7, 8];
      const basesCreditos = creditos.filter(_linea => deudasIds.includes(_linea.tipo.codigo))

      const deudasAhorros = basesCreditos.reduce((total, actual) => {
        return total + actual.saldo;
      }, 0);

      const deudas = creditos.reduce((total, actual) => {
        return total + actual.saldo;
      }, 0);

      const patronal = productos.find(_producto => _producto.codigo.id == 1)
      const obrero = productos.find(_producto => _producto.codigo.id == 2)
      return of({
        savings: (total * 0.9) - deudasAhorros,
        personal: (total * 4 + patronal.monto * 0.5) - deudas,
        mortgage: ((total + patronal.monto * 0.5) * 5) - deudas
      });

    }))
  }


  selectState = (state) => {
    this.stateSelected .next(state);
  }

  selectType = (type) => {
    if (type && type.id == 2) {
      this.states = [
        { id: 1, state: 'En espera' },
        { id: 2, state: 'Cancelado' },
        { id: 3, state: 'Aprobado' },
        { id: 4, state: 'Rechazado' },
        { id: 5, state: 'Contraofertado' },
        { id: 6, state: 'Oferta aceptado' },
        { id: 7, state: 'Oferta rechazada' },
      ]
    } else {
      this.states = [
        { id: 1, state: 'En espera' },
        { id: 2, state: 'Cancelado' },
        { id: 3, state: 'Aprobado' },
        { id: 4, state: 'Rechazado' },
      ]
    }
    this.typeSelected.next(type);
  }

  newPaybill = () => {
    const id = this.fs.createId();
    const socio = this.store.selectSnapshot(UserState.socio)
    this.fs.collection('formalities').doc(id).set({ id, amount: 1000, socio, type: 2, created: Date.now(), state: 1 });
  }

  newSaving = () => {
    const id = this.fs.createId();
    const socio = this.store.selectSnapshot(UserState.socio)
    this.fs.collection('formalities').doc(id).set({ id, amount: 1000, socio, type: 1, created: Date.now(), state: 1 })
  }

  changeState = (formality, state) => {
    this.fs.collection('formalities').doc(formality.id).update({ state })
  }

  active = (formality, state) => {
    this.fs.collection('formalities').doc(formality.id).update({ state })
  }

  cancel = (formality, state) => {
    delete formality.ts;
    this.fs.collection('formalities').doc(formality.id).update({ state })
  }

  getState = (id) =>
    this.states.find(state => state.id === id).state

  getType = (id) =>
    this.types.find(state => state.id === id).type

  addFormality = () => {
    this.dialog.open(NewFormalityComponent, { data: this.Type })
  }

  from = (date) => moment(date).fromNow()


  get Type () {
    return this.typeSelected.value;
  }

  get State () {
    return this.stateSelected.value;
  }


}
