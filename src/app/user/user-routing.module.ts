import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { ServicesComponent } from './services/services.component';
import { AgreementsComponent } from './agreements/agreements.component';
import { FormalitiesComponent } from './formalities/formalities.component';
import { AdminGuard } from '../guards/admin.guard';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: 'services', component: ServicesComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'agreements', component: AgreementsComponent },
      { path: 'formalities', component: FormalitiesComponent },
      { path: 'formalities', component: FormalitiesComponent },
      { path: 'admin', loadChildren: '../admin/admin.module#AdminModule', canActivate: [AdminGuard] },
    ]
  },
  { path: '**', redirectTo: 'services' }

  // { path: 'register', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
