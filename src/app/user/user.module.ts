import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserRoutingModule } from './user-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatBottomSheetModule, MatListModule, MatDialogModule, MatInputModule, MatSnackBarModule, MatSlideToggleModule, MatChipsModule, MatBadgeModule } from '@angular/material';
import { ProductsComponent } from './products/products.component';
import { ServicesComponent } from './services/services.component';
import { AgreementsComponent } from './agreements/agreements.component';
import { FormalitiesComponent } from './formalities/formalities.component';
import { MovementsComponent } from './movements/movements.component';
import { MatRippleModule } from '@angular/material/core';
import { AgreementComponent } from './agreement/agreement.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CrooperComponent } from './crooper/crooper.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ReactiveFormsModule } from '@angular/forms';
import { NewFormalityComponent } from './new-formality/new-formality.component';
import { MatSelectModule } from '@angular/material/select';
import { ChartsModule } from 'ng2-charts-x';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductsComponent,
    ServicesComponent,
    AgreementsComponent,
    FormalitiesComponent,
    MovementsComponent,
    AgreementComponent,
    CrooperComponent,
    NewFormalityComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    UserRoutingModule,
    MatCardModule,
    MatExpansionModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatListModule,
    MatRippleModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    ImageCropperModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatSelectModule,
    ChartsModule,
    MatChipsModule,
    MatBadgeModule,
    MatIconModule
  ],
  entryComponents: [
    MovementsComponent,
    AgreementComponent,
    NewFormalityComponent
  ]
})
export class UserModule { }
