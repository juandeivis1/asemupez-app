import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFormalityComponent } from './new-formality.component';

describe('NewFormalityComponent', () => {
  let component: NewFormalityComponent;
  let fixture: ComponentFixture<NewFormalityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFormalityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFormalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
