import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngxs/store';
import { UserState } from 'src/app/states/user.state';
import { RegistrarTramite } from 'src/app/states/user.actions';

@Component({
  selector: 'app-new-formality',
  templateUrl: './new-formality.component.html',
  styleUrls: ['./new-formality.component.css']
})
export class NewFormalityComponent implements OnInit {
  formality: FormGroup;
  limit: boolean;
  types = [
    { id: 2, type: 'Vale de dinero' },
    { id: 1, type: 'Nuevo monto ahorro navideño' },
    { id: 3, type: 'Nuevo monto ahorro escolar' },
  ]
  constructor(
    private fb: FormBuilder,
    private fs: AngularFirestore,
    private store: Store,
    public dialogRef: MatDialogRef<NewFormalityComponent>,
    @Inject(MAT_DIALOG_DATA) public type: any,
  ) { }

  ngOnInit() {
    const id = this.fs.createId();
    const socio = this.store.selectSnapshot(UserState.socio)
    this.formality = this.fb.group({
      id: [id, [Validators.required]],
      type: [this.type.id, [Validators.required]],
      socio: [socio, [Validators.required]],
      limit: [0, [Validators.required, Validators.max(12)]],
      amount: [null, [Validators.required, Validators.max(150000)]],
      state: [1, [Validators.required]]
    });
    this.setType(this.type)
  }


  add = () => {
    const formality = this.formality.value;
    this.fs.collection('formalities')
      .doc(formality.id)
      .set({ ...formality, created: Date.now() }).then(() => {
        this.dialogRef.close();
        this.store.dispatch(new RegistrarTramite());
      })

  }

  setType = ({ id }) => {
    if (id == 2) {
      this.formality.get('amount').setValidators([Validators.required, Validators.max(150000)])
      this.formality.addControl('limit', this.fb.control(null, [Validators.required]));
      this.limit = true;
    } else {
      this.limit = false;
      this.formality.get('amount').setValidators([Validators.required]);
      this.formality.removeControl('limit');
    }
    this.formality.get('amount').updateValueAndValidity();
  }
}
