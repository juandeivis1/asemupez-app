import { Component, OnInit } from '@angular/core';
import { UserState } from 'src/app/states/user.state';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription, combineLatest, interval, of } from 'rxjs';

import { MatDialog, MatChipInputEvent } from '@angular/material';
import { AgreementComponent } from '../agreement/agreement.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { tap, switchMap, merge } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-agreements',
  templateUrl: './agreements.component.html',
  styleUrls: ['./agreements.component.css']
})
export class AgreementsComponent implements OnInit {
  admin: Observable<boolean>;
  config: Subscription;
  agreements: Observable<any>;
  adminActions: any;
  categoriesList: any;
  categorySelected: string;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  checked: any = true;

  constructor(
    private dialog: MatDialog,
    private fs: AngularFirestore,
    private store: Store
  ) { }

  ngOnInit() {
    this.config = this.fs.collection('agreements').doc('config').valueChanges().pipe(
      tap(({ categories }) => {
        this.categoriesList = categories;
      })
    ).subscribe();;

    this.admin = this.store.select(UserState.admin).pipe(tap(admin => {
      this.adminActions = admin
    }))
  }

  add = () => {
    this.dialog.open(AgreementComponent, { width: '500px', data: { categories: this.categoriesList } })
  }


  edit = (agreement) => {
    this.dialog.open(AgreementComponent, { width: '500px', data: { agreement, categories: this.categoriesList } })
  }

  remove = ({ id }) => {
    this.fs.collection('agreements').doc(id).delete()
  }

  filter = ({ checked }) => {
    this.checked = checked;
    this.agreements = this.fs.collection('agreements', ref => ref.where('img.valid', '==', checked).where('categories', 'array-contains', this.categorySelected).limit(2).orderBy('ts', 'desc')).valueChanges();
  }

  searchAgreements = (category) => {
    this.categorySelected = category;
    this.agreements = this.fs.collection('agreements', ref => ref.where('img.valid', '==', this.checked).where('categories', 'array-contains', this.categorySelected).limit(2).orderBy('ts', 'desc')).valueChanges()
  }

  removeCategory = (category) => {
    this.fs.collection('agreements').doc('config').update({
      categories: firebase.firestore.FieldValue.arrayRemove(category)
    })
  }

  addCategory = (event: MatChipInputEvent) => {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.fs.collection('agreements').doc('config').update({
        categories: firebase.firestore.FieldValue.arrayUnion(value.trim())
      })
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  getMore = (agreements: any[]) => {
    const lastAgreement = [...agreements].pop()
    this.agreements = this.fs.collection<any>('agreements',
      ref =>
        ref
          .where('img.valid', '==', true)
          .where('categories', 'array-contains', this.categorySelected)
          .limit(2)
          .orderBy('ts', 'desc')
          .startAfter(lastAgreement.ts)
    ).valueChanges()
      .pipe(
        switchMap( (_agreements) => {
          return of([...agreements,..._agreements])
        })
      )
    // combineLatest([this.agreements]).pipe(tap(
    //   c => {
    //     console.log(c);

    //   }
    // )).subscribe(c => {
    //   console.log(c);

    // });
    // this.agreements.pipe(
    //   switchMap(x => {
    //     console.log(x);
    //     return of (x)
    //   })
    // ).subscribe(
    //   r => 
    //    console.log(r)

    // )
    // this.agreements.pipe(
    //   merge(this.fs.collection('agreements', ref => ref.where('img.valid', '==', true).where('categories', 'array-contains', this.categorySelected).limit(2)..orderBy('ts', 'desc')).valueChanges())
    // )
  }

  ngOnDestroy(): void {
    this.config.unsubscribe();
  }

}
