import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { UserStateModel, UserState, Movement } from 'src/app/states/user.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.css']
})
export class MovementsComponent implements OnInit {
  @Select(UserState.movements) movements: Observable<Movement[]>
  
  constructor() { }

  ngOnInit() {
  }

}
