import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UIState, InstallApp } from 'src/app/states/ui.state';
import { Observable } from 'apollo-link';
import { UserState, UserStateModel } from 'src/app/states/user.state';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  @Select(UIState.install) install: Observable<any> 
  @Select(UserState.admin) admin: Observable<boolean> 
  @Select(UserState.socio) socio: Observable<boolean> 
  @Select(UserState) user: Observable<UserStateModel> 
  
  constructor(
    private store: Store
  ) { }

  ngOnInit() {
  }

  installApp = () => this.store.dispatch(new InstallApp());

}
