import { Component, OnInit, Input } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MatSnackBar } from '@angular/material';
import { Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { UploadImage } from 'src/app/states/user.state';

@Component({
  selector: 'app-crooper',
  templateUrl: './crooper.component.html',
  styleUrls: ['./crooper.component.css']
})
export class CrooperComponent implements OnInit {
  @Input() id: string;
  @Input() img: string;
  imageChangedEvent: any = null;
  croppedImage: any = null;
  croppedImageFile: any = null;

  constructor(
    private snack: MatSnackBar,
    private store: Store,
    private actions: Actions
  ) { 
    // this.actions.pipe(
    //   ofActionSuccessful(CreateFeatureFile)
    // ).subscribe(() => {
    //   this.dialogRef.close();
    //   this.uploading.close();
    // })

    // this.actions.pipe(
    //   ofActionErrored([CreateFeatureFile, SaveImageRoute, UploadImage])
    // ).subscribe(() => {
    //   this.uploading.close();
    // })
    
  }
  
  ngOnInit() {
    console.log(this.img);
  }

  
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      this.croppedImageFile = event.file
  }

  imageLoaded() {
      // show cropper
  }

  cropperReady() {
      // cropper ready
  }

  loadImageFailed() {
    this.croppedImage = null;
    this.snack.open('Error con el archivo seleccionado', 'Ok', { duration: 3000});
  }

  upload = () => {
    // this.uploading = this.dialog.open(LoadingComponent, { width: '350px', data: 'Subiendo imagen...', disableClose: true })
    this.store.dispatch(new UploadImage(this.croppedImageFile, this.id, this.img));
    this.actions.pipe(
      ofActionSuccessful(UploadImage)
    )
    .subscribe(() => {
      this.imageChangedEvent = null;
      this.croppedImage = null;
      this.croppedImageFile = null;
    })
  }

}
