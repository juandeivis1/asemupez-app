import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrooperComponent } from './crooper.component';

describe('CrooperComponent', () => {
  let component: CrooperComponent;
  let fixture: ComponentFixture<CrooperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrooperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrooperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
