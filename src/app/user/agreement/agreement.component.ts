import { Component, OnInit, inject, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { AddAgreement, UpdateAgreement } from 'src/app/states/user.actions';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css']
})
export class AgreementComponent implements OnInit {
  agreement: FormGroup
  agreements;
  
  constructor(
    private fb: FormBuilder,
    private fs: AngularFirestore,
    private store: Store,
    private actions: Actions,
    public dialogRef: MatDialogRef<AgreementComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
    
    this.agreement = this.fb.group({
      id: [null, []],
      name: [null, [Validators.required]],
      categories: [null, [Validators.required]],
      description: [null, [Validators.required]],
      contact: [null],
      link: [null],
      location: [null]
    });
    if (this.data.agreement) {
      const { id, description, name, img, contact = null, location = null, link = null, categories } = this.data.agreement;
      this.agreement.patchValue({ description, name, id, contact, location, link, categories });
    }
  }

  get img() {
    return this.agreement.get('img') ? this.agreement.get('img').value : null;
  }

  get description() {
    return this.agreement.get('description').value as string;
  }

  get id() {
    return this.agreement.get('id') ? this.agreement.get('id').value : null;
  }

  add = () => {
    const id = this.fs.createId();
    this.store.dispatch(new AddAgreement({ ...this.agreement.value, id, ts: Date.now(), img: { valid: false } }));
    this.actions.pipe(ofActionSuccessful(AddAgreement)).subscribe(() => {
      this.agreement.patchValue({ id });
    })
  }

  update = () => {
    this.store.dispatch(new UpdateAgreement({ ...this.agreement.value, ts: Date.now() }));
    this.actions.pipe(ofActionSuccessful(UpdateAgreement)).subscribe(() => {
      this.dialogRef.close();
    })
  }

}
