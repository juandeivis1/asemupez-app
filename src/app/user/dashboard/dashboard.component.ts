import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/states/user.state';
import { Logout } from 'src/app/states/user.actions';
import { tap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  admin: any;
  adminActions: any;
  formalities: any;


  constructor(
    private store: Store,
    private fs: AngularFirestore
    ) { 
      
    }

  ngOnInit() {
    this.admin = this.store.select(UserState.admin).pipe(tap(admin => {
      this.adminActions = admin
    }))

    this.formalities = this.fs.collection('formalities',formalities => formalities.where('state', '==', 1).limit(5)).valueChanges()
  }

  logout = ( ) => this.store.dispatch( new Logout())
  

}
